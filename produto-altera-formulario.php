<?php include("cabecalho.php");
include("conecta.php");
include("banco-categoria.php");
include("banco-produto.php");

$id = $_GET['id'];
$produto = buscaProduto($conexao, $id);
$categoria = listaCategorias($conexao);
$usado = $produto['usado'] ? "checked='checked'" : ""; //operador ternário, ele verifica a operação booleana e traz dois valores para verdadeiro ou falso

?>
    <h1>Alterando produto</h1>
    <form action="altera-produto.php" method="POST">
        <input type="hidden" name="id" value="<?=$produto['id']?>">
        <table class="table">
            <tr>
                <td>Nome</td>
                <td><input type="text" name="nome" class="form-control" value="<?=$produto['nome']?>"></td>
            </tr>
            <tr>
                <td>Preço</td>
                <td><input type="number" name="preco" class="form-control" value="<?=$produto['preco']?>"></td>
            </tr>
            <tr>
                <td>Descrição</td>
                <td><textarea name="descricao" class="form-control noresize"><?=$produto['descricao']?></textarea></td>
            </tr>
            <tr>
                <td>Usado</td>
                <td><input type="checkbox" name="usado" <?= $usado ?> value="true"></td>
            </tr>
            <tr>
                <td>Categoria</td>
                <td>
                    <select name="categoria_id" class="form-control">
                    <?php
                        $categorias = listaCategorias($conexao);
                        foreach ($categorias as $categoria) :
                        $essaEACategoria = $produto['categoria_id'] == $categoria['id'];
                        $selecao = $essaEACategoria ? "selected='selected'" : "" ; 
                    ?>
                        <option value="<?=$categoria['id']?>" <?=$selecao?>><?=$categoria['nome']?></br>
                    <?php
                        endforeach;
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <!-- <tr>
                <td>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </td>
                <td></td>
            </tr> -->
        </table>
        <button type="submit" class="btn btn-primary">Cadastrar</button>        
    </form>
<?php include("rodape.php") ?>