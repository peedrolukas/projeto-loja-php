<?php

function listaProdutos($conexao) {
    $produtos = array();        //variável produtos é uma array
    $resultado = mysqli_query ($conexao, "select p.*,c.nome as categoria_nome from produtos as p join categorias as c on c.id=p.categoria_id");        //resultado do select
    while ($produto = mysqli_fetch_assoc($resultado)) {        //fetch_assoc, separa linha a linha o resultado da query
        array_push($produtos, $produto);      //array coloca (push) dentro de PRODUTOS o produto da fetch assoc
    }   
    return $produtos;
}

function insereProduto ($conexao, $nome, $preco, $descricao, $categoria_id, $usado) {
    $query = "insert INTO produtos (nome, preco, descricao, categoria_id, usado) values ('{$nome}',{$preco},'{$descricao}',{$categoria_id},{$usado})";     
    return mysqli_query($conexao, $query);
}

function alteraProduto ($conexao, $id, $nome, $preco, $descricao, $categoria_id, $usado) {
    $query = "update produtos set nome = '{$nome}', preco = {$preco}, descricao = '{$descricao}', 
        categoria_id = {$categoria_id}, usado = {$usado} where id = '{$id}'";     
    return mysqli_query($conexao, $query);
}

function buscaProduto ($conexao, $id) {
    $query = "select * from produtos where id = {$id}";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}

function removeProduto ($conexao, $id) {
    $query = "delete FROM produtos WHERE id = {$id}";
    return mysqli_query($conexao, $query);
}

?>

