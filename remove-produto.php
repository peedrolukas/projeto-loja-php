<?php
include ("cabecalho.php");
include ("conecta.php");
include ("banco-produto.php");

$id = $_POST['id'];
removeProduto($conexao, $id);
header("location: lista-produto.php?removido=true");
die();
?>

<p class="text-success">Produto <?php echo $id ?> removido</p>