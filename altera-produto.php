<?php 
    include("cabecalho.php");
    include("conecta.php");
    include("banco-produto.php");
    include("banco-categoria.php");

        $id = $_POST ["id"];
        $nome = $_POST ["nome"];
        $preco = $_POST ["preco"];
        $descricao = $_POST ["descricao"];
        $categoria_id = $_POST ["categoria_id"];

        if(array_key_exists('usado', $_POST)) {
            $usado = "true";
        } else {
            $usado = "false";
        }

        if(alteraProduto($conexao, $id, $nome, $preco, $descricao, $categoria_id, $usado)) {
    ?>
        <p class="text-success">Produto <?php echo $nome; ?>, no valor de <?php echo $preco; ?> alterado com sucesso!</p>
    <?php } else { 
        $error = mysqli_error($conexao);
    ?>
        <p class="text-danger">Produto <?php echo $nome; ?> não pôde ser alterado: <?php echo $error ?> </p>
    <?php
        }
        mysqli_close($conexao);
    include("rodape.php");
?>

    