<?php include("cabecalho.php");
include("conecta.php");
include("banco-categoria.php");
?>
    <h1>Formulário de produtos</h1>
    <form action="adiciona-produto.php" method="POST">
        <table class="table">
            <tr>
                <td>Nome</td>
                <td><input type="text" name="nome" class="form-control"></td>
            </tr>
            <tr>
                <td>Preço</td>
                <td><input type="number" name="preco" class="form-control"></td>
            </tr>
            <tr>
                <td>Descrição</td>
                <td><textarea name="descricao" class="form-control noresize"></textarea></td>
            </tr>
            <tr>
                <td>Usado</td>
                <td><input type="checkbox" name="usado" value="true"></td>
            </tr>
            <tr>
                <td>Categoria</td>
                <td>
                    <select name="categoria_id" class="form-control">
                    <?php
                        $categorias = listaCategorias($conexao);
                        foreach ($categorias as $categoria) :
                    ?>
                        <option value="<?=$categoria['id']?>"><?=$categoria['nome']?></br>
                    <?php
                        endforeach;
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <!-- <tr>
                <td>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </td>
                <td></td>
            </tr> -->
        </table>
        <button type="submit" class="btn btn-primary">Cadastrar</button>        
    </form>
<?php include("rodape.php") ?>